import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

class Runner {

	public static void main(String[] args) {

		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/jdbc_collage", "root",
					"root");
			Statement statement = connection.createStatement();
			int r = statement.executeUpdate("delete from tabel_student where student_id = 4");
		
			
			
			System.out.println("no of rows affected  -> " + (r));
			connection.close();
			System.out.println("connection created");
		} catch (ClassNotFoundException e) {
			System.out.println("connection failed");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
