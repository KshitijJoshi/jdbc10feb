import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

public class PrepereStatementExmaple {

	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);

		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/jdbc_collage", "root",
					"root");

			PreparedStatement prepareStatement = connection
					.prepareStatement(" insert into tabel_student values( ?, ?, ? ) ");
			int i = 0;
			
			while (i < 2) {
				System.out.println("enter the  id ");
				int id = scanner.nextInt();
				prepareStatement.setInt(1, id);
				scanner.nextLine();
				System.out.println("enter the  name ");
				String name = scanner.nextLine();
				prepareStatement.setString(2, name);

				System.out.println("enter the  stream ");
				String stream = scanner.nextLine();
				prepareStatement.setString(3, stream);
				prepareStatement.executeUpdate();
				i++;
			}

		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
