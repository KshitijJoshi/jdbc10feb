import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class executeExample {

	public static void main(String[] args) {
		

		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/jdbc_collage", "root",
					"root");

			Statement statement = connection.createStatement();
			
			boolean executeSelectQuery = statement.execute("select * from tabel_student");
			//boolean executeNonSlect = statement.execute("delete from tabel_student where student_id = 1");
			
			ResultSet resultSet = statement.getResultSet();
			while(resultSet.next()) {
				System.out.println(resultSet.getString("student_name"));
			}
			
			
			
			
			System.out.println("executeSelectQuery " +executeSelectQuery );
			

		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

}
